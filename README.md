# Bingo Generator

Takes an array of values (in this case, strings that represent things one might see on NextDoor) and randomly generates a printable bingo sheet.  Refresh and print as many as you need.  If you want to make your own, just change the array values. NOTE: You must have at least as many values as there are squares, the code removes the values from the array when they are read to prevent duplicates. If you don't have enough, it won't be able to populate the board.

# Instructions

Gather some people (preferably your neighbors), enough markers for everyone, and print out enough sheets for everyone.
The officiator will hand out the sheets and markers, and open NextDoor on his device.
The center square is "Free" so everyone can mark it when the game begins.
The officiator reads the posts from newest to oldest (it isn't neccessary to read them aloud, but if they're funny, feel free to do so!) and picks one of the categories below to which the post belongs. (It is possible that it belongs to 2 categories, so you'll either need to announce both, or pick the one it fits best.)  Call out categories as you get to them in newest to oldest order.
If a player has that category, (s)he marks it off as it is called. 
The game ends when someone marks off a complete horizontal or vertical line, or a diagonal that goes from a corner through the center (free) square and yells Bingo or "I win the neighborhood!" or whatever, it's your party.  :-)

## Categories
Activism Post
Package Stolen
Kid wanting to Mow
Speeder complaints
Lost Cat
Lost Dog
Car Stolen
Car Broken into
House Broken into
Vandalism
Coyote Spotted
Lost farm animal
Found cat
Found dog
Found farm animal
Noise complaint
Trouble children
Police Activity
Free stuff nobody should want
Murder
Looking for free expensive thing
Community Festival/Event
Church/Religious Festival/Event
School Festival/Event
Weird shit
Drug Bust
Complaints about cannabis
Complaints about alcohol
Emergency Post that isn't an emergency